# Amazon EC2 cluster

## About

You can deploy a go-docker cluster in Amazon EC2 with this contextualization
code.

Scripts have been tested in Debian 8, with cloud-init. For other systems,
scripts should be adapted.

Default configuration is in /opt/go-docker/docker-files/prod/go-d.ini and
production.ini

It is based on Docker swarm, and uses local authentication. You can use the
godocker user to login (password: godocker). Scripts create a local godocker
user and its home in a directory shared with nodes (uid, gid: 1005).
You can change his id or password in the scripts.

This configuration is for testing/demo purpose only, not production.

To use a different configuration, simply clone go-docker repository and update
go-d.ini.sample file to fit your config. Then, update the master.sh script at
the following lines to match your own repository/config file:

    # Specify your cloned/updated repo
    git clone https://bitbucket.org/osallou/go-docker.git
    cp /opt/go-docker/docker-files/prod/go-d.ini.sample /opt/go-docker/docker-files/prod/go-d.ini


## Master

Install master using contextualization from script master.sh

You should open port 6543 on the virtual machine to access the web server.

When ready, connect to the VM and look at file content /var/log/godocker.log.
It contains the master local ip address and cluster id. Note those for node
installation.

## Node

At least one node is needed, the master does not manage computation tasks.

Install slave using contextualization. Replace in the script the master ip and
cluster id values, using script slave.sh

Nodes declare themselves to Docker Swarm on master. You can dynamically add or
remove nodes in the cluster.
