#!/bin/bash

echo "Install base"

apt-get update
apt-key adv --keyserver hkp://pgp.mit.edu:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D
echo "deb https://apt.dockerproject.org/repo debian-jessie main" > /etc/apt/sources.list.d/docker.list
apt-get update
apt-get install -y docker-engine
service docker restart
docker pull swarm
mkdir /opt/godshared

echo "Install Master"

apt-get install -y nfs-kernel-server git
echo "/opt/godshared  *(rw)" | tee --append /etc/exports
service nfs-kernel-server restart
clusterid=`docker run --rm swarm create`
localip=`wget -qO- http://169.254.169.254/latest/meta-data/local-ipv4`
echo "Local IP: "$localip > /var/log/godocker.log
echo "Cluster ID: "$clusterid >> /var/log/godocker.log
docker run -d -p 2375:2375 --name god-swarm swarm manage token://$clusterid

docker run --name god-mongo -d mongo
docker run --name god-redis -d redis

sleep 10

cd /opt
git clone https://bitbucket.org/osallou/go-docker.git

cp /opt/go-docker/docker-files/prod/go-d.ini.sample /opt/go-docker/docker-files/prod/go-d.ini

sed -i "s;auth_policy:.*;auth_policy: 'local';g" /opt/go-docker/docker-files/prod/go-d.ini

groupadd -g 1005 godocker
useradd --uid 1005 --gid 1005 --home /opt/godshared/home/godocker godocker
mkdir -p /opt/godshared/home/godocker
chown -R godocker:godocker /opt/godshared/home/godocker

docker run \
  --link god-mongo:god-mongo \
  --link god-redis:god-redis \
  -v /opt/godshared:/opt/godshared \
  -v /opt/go-docker/docker-files/prod/go-d.ini:/opt/go-docker/go-d.ini \
  --rm \
  osallou/go-docker \
  /usr/bin/python seed/create_local_user.py --config /opt/go-docker/go-d.ini --login godocker --password godocker --uid 1005 --gid 1005 --home /opt/godshared/home/godocker


docker run \
  -d \
  --name god-web \
  --link god-mongo:god-mongo  \
  --link god-redis:god-redis  \
  --link god-swarm:god-swarm  \
  -v /opt/godshared:/opt/godshared \
  -v /opt/go-docker/docker-files/prod/go-d.ini:/opt/go-docker/go-d.ini \
  -v /opt/go-docker/docker-files/prod/production.ini:/opt/go-docker-web/production.ini \
  -p 6543:6543 \
  -e "PYRAMID_ENV=prod" \
  osallou/go-docker \
  gunicorn -p godweb.pid --log-config=/opt/go-docker-web/production.ini --paste /opt/go-docker-web/production.ini


docker run \
  --link god-mongo:god-mongo \
  --link god-redis:god-redis \
  --link god-swarm:god-swarm \
  -v /opt/godshared:/opt/godshared \
  -v /opt/go-docker/docker-files/prod/go-d.ini:/opt/go-docker/go-d.ini \
  --rm \
  osallou/go-docker \
  /usr/bin/python go-d-scheduler.py init

docker run \
  -d \
  --link god-mongo:god-mongo  \
  --link god-redis:god-redis  \
  --link god-web:god-web \
  --link god-swarm:god-swarm \
  -v /opt/godshared:/opt/godshared \
  -v /opt/go-docker/docker-files/prod/go-d.ini:/opt/go-docker/go-d.ini \
  osallou/go-docker \
  /usr/bin/python go-d-scheduler.py run

docker run \
  -d \
  --link god-mongo:god-mongo  \
  --link god-redis:god-redis  \
  --link god-web:god-web \
  --link god-swarm:god-swarm \
  -v /opt/godshared:/opt/godshared \
  -v /opt/go-docker/docker-files/prod/go-d.ini:/opt/go-docker/go-d.ini \
  osallou/go-docker \
  /usr/bin/python go-d-watcher.py run

