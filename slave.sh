#!/bin/bash

# UPDATE FOLLOWING FIELDS WITH MASTER INFO (/var/log/go-docker.log)
clusterid=
masterip=

echo "Install base"
apt-get update
apt-key adv --keyserver hkp://pgp.mit.edu:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D
echo "deb https://apt.dockerproject.org/repo debian-jessie main" > /etc/apt/sources.list.d/docker.list
apt-get update
apt-get install -y docker-engine
service docker restart
docker pull swarm
mkdir /opt/godshared

echo "Install Node"

apt-get install -y nfs-common

echo "DOCKER_OPTS= \"-H 0.0.0.0:2375\"" | tee --append /etc/default/docker
sed -i "s;ExecStart=.*;ExecStart=/usr/bin/docker daemon -H 0.0.0.0:2375;g" /lib/systemd/system/docker.service
systemctl daemon-reload
service docker restart

localip=`wget -qO- http://169.254.169.254/latest/meta-data/local-ipv4`
docker -H 127.0.0.1:2375 run -d swarm join --advertise=$localip:2375 token://$clusterid

mount -t nfs  $masterip:/opt/godshared /opt/godshared

groupadd -g 1005 godocker
useradd --uid 1005 --gid 1005 --home /opt/godshared/home/godocker godocker


docker -H 127.0.0.1:2375 pull google/cadvisor
docker -H 127.0.0.1:2375 run \
  --volume=/:/rootfs:ro \
  --volume=/var/run:/var/run:rw \
  --volume=/sys:/sys:ro \
  --volume=/var/lib/docker/:/var/lib/docker:ro \
  --publish=8080:8080 \
  --detach=true \
  --name=cadvisor \
  google/cadvisor:latest
